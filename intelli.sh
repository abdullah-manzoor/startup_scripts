#!/bin/sh
. /etc/rc.d/init.d/functions
service=mysql
service1=app.js
USER=root

sleep 60
if (( $(ps -ef | grep -v grep | grep $service | wc -l) > 0 ))
then
        echo "$service is running!!!, Starting Intellicon Event Server"
        if (( $(ps -ef | grep -v grep | grep $service1 | wc -l) > 0 ))
        then
                echo "$service1 is already running!!! No need to start"
        else
                echo "$service1 is not running, Starting Intellicon Event Server"
                #sleep 10
                #cd /etc/node/
                #/usr/bin/node /etc/node/starter.js
                runuser -l "$USER" -c "cd /etc/node/ && /usr/local/bin/node /etc/node/starter.js"
                pm2 stop all && pm2 delete all
                runuser -l "$USER" -c "cd /etc/node/ && /usr/local/bin/node /etc/node/starter.js"
        fi
else
        echo "$service is not running"
        if (( $(ps -ef | grep -v grep | grep $service1 | wc -l) > 0 ))
        then
                echo "$service1 is already running!!! Stopping Intellicon Event Server"
                #cd /etc/node/ && /usr/bin/node /etc/node/stopper.js
                runuser -l "$USER" -c "cd /etc/node/ && /usr/local/bin/node /etc/node/stopper.js"
        else
                echo "$service1 is not running"
        fi
fi