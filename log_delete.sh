#!/bin/bash
# This script will delete the logs older then 30 day from the /var/log/intellicon directory
# This script will delete the logs older then 60 day from /var/log/*
# This script only delete the files not the folder
# Cron job expression for this script ( 0 0 1 * * /var/local/logs_delete.sh >> /var/local/logs_delete.log)
# Per Week Cron job expression for this script ( 0 0 * * Sun /var/local/logs_delete.sh >> /var/local/logs_delete.log)
# This script will run every month on 1st day at 00:00:00.

find /var/log/intellicon/* -mtime +7 -type f -delete
find /root/.pm2/logs/* -mtime +7 -type f -delete
find /var/log/asterisk/* -mtime +30 -type f -delete
find /var/log/httpd/* -mtime +30 -type f -delete
find /var/log/nginx/* -mtime +30 -type f -delete
find /var/log/mongodb/* -mtime +30 -type f -delete
find /var/log/* -mtime +30 -type f -delete
rm -rf /var/log/turn_*