#!/bin/sh

#This service script created by: Abdullah Manzoor
#Department:					 Customer Success Team
#Cron Job for cluster */1 * * * * /var/local/logger.sh  >> /var/local/srvscript.log
#Cron Job on Reboot @reboot /var/local/logger.sh  >> /var/local/srvscript.log

. /etc/rc.d/init.d/functions
service=rabbitmq
service1=subscriber.js
service2=publisher.js
USER=root

sleep 30
if (( $(ps -ef | grep -v grep | grep $service | wc -l) > 0 )) 
then
        echo "$service is running!!!, Starting Intellicon Logger services"
		if (( $(ps -ef | grep -v grep | grep $service1 | wc -l) > 0 ))
        then
                echo "$service1 is already running!!! No need to start"
		
        else
                echo "$service1 is not running, Starting Intellicon Logger services"
                runuser -l "$USER" -c "systemctl start intellilogsub.service"
		
        fi

        if (( $(ps -ef | grep -v grep | grep $service2 | wc -l) > 0 ))
        then
                echo "$service2 is already running!!! No need to start"
        else
                echo "$service2 is not running, Starting Intellicon Logger services"
                runuser -l "$USER" -c "systemctl start intellilogpub.service"

        fi

else
        echo "$service is not running"
        if (( $(ps -ef | grep -v grep | grep $service1 | wc -l) > 0 )) && (( $(ps -ef | grep -v grep | grep $service2 | wc -l) > 0 ))
        then
                echo "$service1 is not running!!! Stopping Intellicon Logger services"
                runuser -l "$USER" -c "systemctl stop intellilogsub.service"
                runuser -l "$USER" -c "systemctl stop intellilogpub.service"
		
        else
                echo "$service1 is not running"
        fi
fi
# RABBITMQ-SERVER is enabled on startup