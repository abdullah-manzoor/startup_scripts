#!/bin/sh

#This service script created by: Abdullah Manzoor
#Department:					 Customer Success Team
#Cron Job for cluster */1 * * * * /var/local/opensips.sh  >> /var/local/srvscript.log
#Cron Job on boot @reboot /var/local/opensips.sh  >> /var/local/srvscript.log

. /etc/rc.d/init.d/functions
service=rtpengine
service1=opensips
USER=root

sleep 30
if (( $(ps -ef | grep -v grep | grep $service | wc -l) > 0 )) 
then
        echo "$service is running!!!, Starting Opensips service"
		if (( $(ps -ef | grep -v grep | grep $service1 | wc -l) > 0 ))
        then
                echo "$service1 is already running!!! No need to start"
        else
                echo "$service1 is not running, Starting Opensips service"
                runuser -l "$USER" -c "systemctl start opensips.service"
		
        fi
else
        echo "$service is not running"
        if (( $(ps -ef | grep -v grep | grep $service | wc -l) > 0 ))
        then
                echo "$service is not running!!! Stopping Opensips service"
                runuser -l "$USER" -c "systemctl stop opensips.service"
        else
                echo "$service1 is not running"
        fi
fi