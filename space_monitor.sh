#!/bin/bash

MOUNT_POINT="/"
USED_SPACE=$( df -h $MOUNT_POINT | awk '{ print $5 }' | sort -n | tail -n 1 | sed 's/%//' )
PM2="/root/.pm2/logs"
INTELLICON=/var/log/intellicon
ASTERISK=/var/log/asterisk
LOGS=/var/log

LOWER_LIMIT=80
HIGHER_LIMIT=90

LIMT_FILE=/var/local/set_limit
SET_LIMT=`cat $LIMT_FILE`

function _delete_recordings() {

for YEAR in /var/spool/asterisk/monitor/* ; do
    if [ -d "$YEAR" ]; then
                cd $YEAR
                for MONTH in $YEAR/* ; do
                        if [ -d "$MONTH" ]; then
                                cd $MONTH
                                for DAY in $MONTH/* ; do
                                        if [ -d "$DAY" ]; then
                                                echo Check the DISK Space Again
                                                sleep 1
                                                for DISK in "$USED_SPACE" ; do
                                                        if [ "$DISK" -le "$LOWER_LIMIT" ]; then
                                                              echo "Disk Space is Less than from ${LOWER_LIMIT}% : ${USED_SPACE}%"
                                                              echo Exiting....!!!
                                                              exit
                                                        else
                                                          echo Disk Spacke is: ${USED_SPACE}%
                                                          echo Going to delete $DAY
                                                          echo Going to delete $PM2 files
                                                          echo Going to delete $INTELLICON files
														  echo Going to delete $ASTERISK files
														  /usr/sbin/asterisk -rx "logger rotate"
														  find $ASTERISK/* -mtime +15 -type f -delete
														  find $LOGS/* -mtime +30 -type f -delete
														  rm -rf $PM2/*
                                                          rm -rf $INTELLICON/*
                                                          rm -rf $DAY
                                                          USED_SPACE_AFTER=$( df -h $MOUNT_POINT | awk '{ print $5 }' | sort -n | tail -n 1 | sed 's/%//' )
														  echo "Spache Free After: $USED_SPACE_AFTER"
                                                          exit
                                                        fi
                                                done
                                        fi
                                done
                        fi
                done
        fi
done
}

if [ $USED_SPACE -le $HIGHER_LIMIT ]; then
		echo "Nothing to Do Disk Space is Good"
else
		echo Going to Run the Fucntion
		echo "Disk Space is: ${USED_SPACE}% that is Greater Than ${HIGHER_LIMIT}%"
		_delete_recordings
fi