#!/bin/sh
. /etc/rc.d/init.d/functions
service=mysql
service1=turnserver
USER=root

sleep 60
if (( $(ps -ef | grep -v grep | grep $service | wc -l) > 0 ))
then
        echo "$service is running!!!, Starting turnserver Server"
        if (( $(ps -ef | grep -v grep | grep $service1 | wc -l) > 0 ))
        then
                echo "$service1 is already running!!! No need to start"
        else
                echo "$service1 is not running, Starting turnserver Server"
                
                runuser -l "$USER" -c "nohup /usr/local/bin/turnserver &"
        fi
else
        echo "$service is not running"
        if (( $(ps -ef | grep -v grep | grep $service1 | wc -l) > 0 ))
        then
                echo "$service1 is already running!!! Stopping turnserver Server"
                runuser -l "$USER" -c "nohup /usr/local/bin/turnserver &"
        else
                echo "$service1 is not running"
        fi
fi